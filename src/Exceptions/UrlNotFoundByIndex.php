<?php

namespace Acanto\Crawler\Exceptions;

use RuntimeException;

class UrlNotFoundByIndex extends RuntimeException
{
}
